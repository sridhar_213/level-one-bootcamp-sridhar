#include<stdio.h>
#include<math.h>
typedef struct 
{
	float x;
	float y;
}Coordinates;
Coordinates readval(int n)
{
	Coordinates i;
	printf("Enter the x coordinate of point %d\n",n);
	scanf("%f",&i.x);
	printf("Enter the y coordinate of point %d\n",n);
	scanf("%f",&i.y);
	return i;
}
float compdist(Coordinates p1, Coordinates p2)
{
	float dist;
	dist = sqrt( (pow((p2.x-p1.x),2))+(pow((p2.y-p1.y),2)));
	return dist;
}
void displaydist(Coordinates p1, Coordinates p2, float dist)
{
	printf("The distance between %.2f, %.2f and %.2f,%.2f is: %.2f\n", p1.x, p1.y, p2.x, p2.y, dist);
}
int main()
{
	Coordinates p1,p2;
	float dist;
	p1=readval(1);
	p2=readval(2);
	dist=compdist(p1,p2);
	displaydist(p1,p2,dist);
	return 0;
}