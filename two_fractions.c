#include <stdio.h>
typedef struct 
{int n,d;
    
}fraction;
 
fraction input(int n)
{
    fraction f;
    printf("Numerator for fraction%d\n",n);
    scanf("%d",&f.n);
    printf("Denominator for fraction %d\n",n);
    scanf("%d",&f.d);
    return f;
}
int gcd(int n,int d)
{
    int temp;
    while(n)
    {
        temp=n;
        n=d%n;
        d=temp;
    }
    return d;
}
fraction compute(fraction sum,fraction f1,fraction f2)
{
    sum.n=(f1.n*f2.d)+(f2.n*f1.d);
    sum.d=f1.d*f2.d;
    int g;
    g=gcd(sum.n,sum.d);
    sum.n=sum.n/g;
    sum.d=sum.d/g;
    return sum;
}
void output(fraction f1,fraction f2,fraction sum)
{
    printf("The sum of %d %d and %d %d is %d/%d\n",f1.n,f1.d,f2.n,f2.d,sum.n,sum.d);
 
}
int main()
{
    fraction f1,f2,sum;
    f1=input(1);
    f2=input(2);
    sum=compute(sum,f1,f2);
    output(f1,f2,sum);
}

